﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UploadFileApplication.Models;

namespace UploadFileApplication.Controllers
{
    public class MainController : Controller
    {
        public enum State {calculated,uncalculated};
        // GET: Main
        public ActionResult Index()
        {
            ViewBag.State = State.uncalculated;
            return View();
        }

        public void UploadList(dynamic[] list) {
            List<NodeStructure> NodeList = new List<NodeStructure>();

            foreach (dynamic item in list)
            {
                NodeStructure node;
                NodeStructure.TryParse(item, out node);

                if (node != null)
                    NodeList.Add(node);
            }
            ViewBag.UpdatedList = NodeList;
        }
    }
}