﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UploadFileApplication.Models
{
    public class NodeStructure
    {
        string name;
        int weight;
        NodeStructure left, right;

        public NodeStructure(string name, int weight, NodeStructure left, NodeStructure right)
        {
            Name = name;
            Weight = weight;
            Left = left;
            Right = right;
        }

        public NodeStructure() { }

        public string Name { get => name; set => name = value; }
        public int Weight { get => weight; set => weight = value; }
        public NodeStructure Left { get => left; set => left = value; }
        public NodeStructure Right { get => right; set => right = value; }

        public static void TryParse(dynamic nodeStructureDynamic,out NodeStructure nodeStructure)
        {
            try
            {
                NodeStructure _left,_right;
                TryParse(nodeStructureDynamic.left, out _left);
                TryParse(nodeStructureDynamic.left, out _right);

                nodeStructure = new NodeStructure()
                {
                    Name = nodeStructureDynamic.name,
                    Weight = nodeStructureDynamic.weight,
                    Left = _left,
                    Right = _right
                };
            }
            catch (Exception ex)
            {
                nodeStructure = null;
            }
        }
    }
}